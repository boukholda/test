<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/add_transport', 'TransportController@add_transport')->name('add_transport');
Route::get('/add_companies', 'CompaniesController@add_companies')->name('add_companies');
Route::get('/add_parking', 'ParkingController@add_parking')->name('add_parking');

//Route::get('/', TransportController::class,'add_transport')->name('new');

//Route::post('/save', TransportController::class)->name('save');
