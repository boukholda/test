<?php

namespace App\Http\Controllers;
use  App\Models\Transport;
use Illuminate\Http\Request;

class TransportController extends Controller
{
    public function add_transport(){
    return view('add_transport');
    }
    public function add(Request $request)
    {
        $this->validate($request,[
            'name'=>'required|max:100',
            'type'=>'required|max:100',
        ]);
        try{

            $name =$request->name;
            $type=$request->type;
            $data=[
                'name'=>$name,
                'type'=>$type,
            ];
            $Transport = new Transport();
            $Transport->name =$name;
            $Transport->type= $type;
            $Transport->save();
            return dd('Data');
           return redirect()->back()->with('insert','Data has been insert successfully');

        }
        catch (\Exception $e){
            return  redirect()->back()->with('error','Data has been insert faill.');

        }
    }
}
