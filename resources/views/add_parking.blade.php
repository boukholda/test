@extends('layouts.dashboard')

@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Dashboard</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Parking</a></li>
                        <li class="breadcrumb-item active">Manage Parking </li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <div class="signup-form">
        <form action="" method="post" class="form-horizontal">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-8 offset-4">
                    <h2>Parking</h2>
                </div>
            </div>
            {{-- success --}}
            @if(\Session::has('insert'))
                <div id="insert" class=" alert alert-success">
                    {!! \Session::get('insert') !!}
                </div>
            @endif

            {{-- error --}}
            @if(\Session::has('error'))
                <div id="error" class=" alert alert-danger">
                    {!! \Session::get('error') !!}
                </div>
            @endif


            <div class="form-group row">
                <label class="col-form-label col-4">Full Name</label>
                <div class="col-8">
                    <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" placeholder="Enter Name">
                    @error('name')
                    <span class="invalid-feedback" role="alert">
						<strong>{{ $message }}</strong>
					</span>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label class="col-form-label col-4">Type</label>
                <div class="col-8">
                    <input type="text" class="form-control @error('type') is-invalid @enderror" name="type"  placeholder="Enter Type">
                    @error('type')
                    <span class="invalid-feedback" role="alert">
							<strong>{{ $message }}</strong>
						</span>
                    @enderror
                </div>
            </div>
            <div class="form-group row">
                <div class="col-8 offset-4">
                    <button type="submit" class="btn btn-primary btn-lg">Save</button>
                </div>
            </div>
        </form>

    </div>
    {{-- <table> --}}
    <div class="container">
        <div class="container-fluid">
            <table id="example" class="table table-striped table-bordered nowrap" style="width:100%">
                <thead>

                <tr>
                    <th>ID</th>
                    <th>Full Name</th>
                    <th>Type of Parking</th>
                    <th>Modefy</th>
                </tr>
                </thead>
                <tbody>


                </tbody>
            </table>
        </div>
    </div>
    {{-- </table> --}}

    <!-- Modal Update-->
    <div class="modal fade" id="update" tabindex="-1" aria-labelledby="update" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="update">Update</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="update" action="" method = "post"><!-- form add -->
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <input type="hidden" class="form-control" id="e_id" name="id" value=""/>
                        <div class="modal-body">
                            <div class="form-group row">
                                <label for="" class="col-sm-3 col-form-label">Full Name</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="e_name" name="name" required="" value=""/>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="" class="col-sm-3 col-form-label">Type</label>
                                <div class="col-sm-9">
                                    <input type="tel" class="form-control" id="e_type" name="type" required="" value=""/>
                                </div>
                            </div>
                        </div>
                        <!-- form add end -->
                    </div>
                    <div class="modal-footer">
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="icofont icofont-eye-alt"></i>Close</button>
                            <button type="submit" id=""name="" class="btn btn-success  waves-light"><i class="icofont icofont-check-circled"></i>Update</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End Modal Update-->

    {{-- script update --}}

    <script>
        $(document).on('click','.update',function()
        {
            var _this = $(this).parents('tr');
            $('#e_id').val(_this.find('.id').text());
            $('#e_name').val(_this.find('.name').text());
            $('#e_type').val(_this.find('.type').text());
        });
    </script>

    {{-- hide message js --}}
    <script>
        $('#insert').show();
        setTimeout(function()
        {
            $('#insert').hide();
        },5000);

        $('#error').show();
        setTimeout(function()
        {
            $('#error').hide();
        },5000);

    </script>
@endsection
